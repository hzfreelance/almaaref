<?php

define('STATUS_SUCCESS', 1);
define('STATUS_FAIL', -1);

define('MESSAGE_GENERAL_CREATE_SUCCESS', 'تم ادراج المعلومات بنجاح');
define('MESSAGE_GENERAL_CREATE_ERROR', 'حصل خطأ ما, أثناء عملية ادراج المعلومات, الرجاء اعادة المحاولة');
define('MESSAGE_GENERAL_UPDATE_SUCCESS', 'تم تعديل المعلومات بنجاح');
define('MESSAGE_GENERAL_UPDATE_ERROR', 'حدث خطأ ما, أثناء عملية تعديل المعلومات, الرجاء اعادة المحاولة');

define('MESSAGE_GENERAL_SUCCESS', '');
define('MESSAGE_GENERAL_ERROR', '');