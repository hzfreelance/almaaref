<?php

namespace App\Http\Controllers\Api;

use App\Http\Repositories\ArticleCategoryRepository;
use App\Http\Repositories\ArticleRepository;
use App\Http\Repositories\BannerRepository;
use App\Http\Repositories\BookCategoryRepository;
use App\Http\Repositories\BookRepository;
use App\Http\Repositories\SectionRepository;
use App\Models\Article;
use Illuminate\Http\Request;
use Validator;

class ApiController extends ApiParentController
{

    /**
     * @var BookRepository
     */
    protected $book;
    /**
     * @var BookCategoryRepository
     */
    protected $bookCategory;
    /**
     * @var Request
     */
    protected $request;

    /**
     * ApiController constructor.
     * @param Request $request
     * @param BookRepository $book
     * @param BookCategoryRepository $bookCategory
     */
    public function __construct(Request $request, BookRepository $book, BookCategoryRepository $bookCategory, SectionRepository $section, BannerRepository $banner, ArticleCategoryRepository $articleCategoryRepository, ArticleRepository $article)
    {
        parent::__construct($request);
        $this->book = $book;
        $this->request = $request;
        $this->bookCategory = $bookCategory;
        $this->section = $section;
        $this->banner = $banner;
        $this->articleCategory = $articleCategoryRepository;
        $this->article = $article;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBooks()
    {
        $request = $this->request;
        $response = $this->book->ApiGetBooks($request->all());
        if ($response)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $response);

        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function findABook()
    {
        $request = $this->request;
        $response = $this->book->ApiFindABook($request->all());
        if ($response)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $response);

        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBook($id)
    {
        $response = $this->book->ApiGetBook($id);
        if ($response)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $response);

        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBooksByCategories()
    {
        $request = $this->request;

        $response = $this->bookCategory->ApiGetBooksByCategories($request->all());
        if ($response)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $response);

        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoryWithBooks($id)
    {
        $request = $this->request;

        $response = $this->bookCategory->ApiGetCategoryWithBooks($id, $request->all());
        if ($response)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $response);

        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getBookChapters($bid)
    {
        $book = $this->book->getOnebook($bid);
        if (isset($book->chapters))
            return $this->success(MESSAGE_GENERAL_SUCCESS, $book->chapters);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getBookChapterPages($cid)
    {
        $chapter = $this->book->viewChapter($cid);
        if (isset($chapter->pages))
            return $this->success(MESSAGE_GENERAL_SUCCESS, $chapter->pages);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getBookChapterPage($pid)
    {
        $page = $this->book->viewPage($pid);
        if ($page)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $page);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getSections()
    {
        $sections = $this->section->listSections();
        if ($sections)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $sections);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getSectionBooks($id)
    {
        $section = $this->section->getSection($id);
        if ($section)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $section->books);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getBookDetails($id)
    {
        $book = $this->book->getBookDetails($id);
        if ($book)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $book);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getLastBanner()
    {
        $banner = $this->banner->getLatestBanner();
        if ($banner)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $banner);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getCategoriesLevels($library_id)
    {
        if ($library_id == 2) {
            $categories = $this->bookCategory->getCategoriesLevels(true);
        }

        if ($library_id == 3) {
            $categories = $this->articleCategory->getCategoriesLevels(true);
        }
        if ($categories)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $categories);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getArticles()
    {
        $articles = $this->article->ApiGetArticles($this->request->all());
        if ($articles)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $articles);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }

    public function getArticle($id)
    {
        $article = $this->article->apiGetArticle($id);
        if ($article)
            return $this->success(MESSAGE_GENERAL_SUCCESS, $article);
        return $this->fails(MESSAGE_GENERAL_ERROR);
    }
}
