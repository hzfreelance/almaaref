<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ApiParentController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function success($message = '',$data = [], $status = 200){
        return response()->json([
            'status' => STATUS_SUCCESS,
            'message' => $message,
            'data' => $data
        ],$status);
    }

    public function fails($message = '',$status = 200){
        return response()->json([
            'status' => STATUS_FAIL,
            'message' => $message,
            'data' => [],
        ], $status);
    }

}
