<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\ArticleCategoryRepository;
use App\Http\Repositories\ArticleRepository;
use App\Http\Repositories\LibraryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ArticleController extends Controller
{
    public function __construct(Request $request, ArticleRepository $article, LibraryRepository $library, ArticleCategoryRepository $articleCategoryRepository)
    {
        parent::__construct($request);
        $this->article = $article;
        $this->library = $library;
        $this->category = $articleCategoryRepository;
    }

    public function index()
    {
        $data = $this->article->list();
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        return view('article.index', compact('data', 'breadcrumb'));
    }

    public function create()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        $libraries = $this->library->list();
        $categories = $this->category->getCategories();
        return view('article.create', compact('breadcrumb', 'libraries', 'categories'));
    }

    public function edit($id)
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        $libraries = $this->library->list();
        $article = $this->article->one($id);
        $categories = $this->category->getCategories();
        return view('article.edit', compact('breadcrumb', 'libraries', 'article', 'categories'));
    }

    public function store()
    {
        $data = $this->request->all();
        $v = Validator::make($data,[
            'title' => 'required',
            'content' => 'required'
        ]);

        if($v->fails()) return redirect()->back()->withErrors($v);

        $response = $this->article->store($data);
        if ($response) {
            return back()->with('success', MESSAGE_GENERAL_UPDATE_SUCCESS);
        } else {
            return redirect()->back()->with('error', MESSAGE_GENERAL_UPDATE_ERROR);
        }

    }

    public function update($id)
    {
        $data = $this->request->all();
        $v = Validator::make($data,[
            'title' => 'required',
            'content' => 'required'
        ]);

        if($v->fails()) return redirect()->back()->withErrors($v);

        $response = $this->article->update($id, $data);
        if ($response) {
            return back()->with('success', MESSAGE_GENERAL_UPDATE_SUCCESS);
        } else {
            return redirect()->back()->with('error', MESSAGE_GENERAL_UPDATE_ERROR);
        }

    }
}
