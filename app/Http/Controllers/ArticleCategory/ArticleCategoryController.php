<?php

namespace App\Http\Controllers\ArticleCategory;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\ArticleCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ArticleCategoryController extends Controller
{

    protected $request;

    public function __construct(Request $request,ArticleCategoryRepository $bookCategory)
    {
        parent::__construct($request);
        $this->request = $request;
        $this->bookCategory = $bookCategory;
    }

    public function index(){
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $categories = $this->bookCategory->list();
        return view('article-category.index', compact('categories', 'breadcrumb'));
    }

    public function store(){
        $request = $this->request;

        $rules = [
            'name' => 'required|unique:book_categories,name',
            'status' => 'required|in:0,1',
            'image' => 'image'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json([
                'status' => -1,
                'data' => $validator->messages()
            ]);
        }

        $response = $this->bookCategory->store($request->all());

        if($response){
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);
        }else{
            return response()->json([
                'status' => -1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_ERROR
                ]
            ]);
        }

    }

    public function edit($id){
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $category = $this->bookCategory->getOne($id);
        $categories = $this->bookCategory->list();
        return view('article-category.edit', compact('category', 'breadcrumb', 'categories'));
    }

    public function update($id){

        $category = $this->bookCategory->getOne($id);

        $request = $this->request;

        $rules = [
            'name' => 'required|unique:book_categories,name,'.$category->id,
            'status' => 'required|in:0,1',
            'image' => 'image'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $response = $this->bookCategory->update($request->all(),$id);

        if($response){
            return redirect()->back()->with('success', MESSAGE_GENERAL_UPDATE_SUCCESS);
        }else{
            return redirect()->back()->with('error', MESSAGE_GENERAL_UPDATE_ERROR);
        }

    }

}
