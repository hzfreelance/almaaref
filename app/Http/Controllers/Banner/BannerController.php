<?php

namespace App\Http\Controllers\Banner;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\BannerRepository;
use App\Http\Repositories\BookRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function __construct(Request $request, BannerRepository $bannerRepository, BookRepository $book)
    {
        parent::__construct($request);
        $this->bannerRepository = $bannerRepository;
        $this->book = $book;
    }

    public function index()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        $data = $this->bannerRepository->getBanners();
        $booksList = $this->book->list();
        return view('banner.index', compact('breadcrumb', 'data', 'booksList'));
    }

    public function show($id)
    {

    }

    public function store()
    {
        $data = $this->request->all();
        $response = $this->bannerRepository->store($data);
        if ($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

    public function update($id)
    {
        $data = $this->request->all();
        $response = $this->bannerRepository->update($id,$data);
        if ($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

}
