<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Repositories\BookAuthorRepository;
use App\Http\Repositories\BookCategoryRepository;
use App\Http\Repositories\BookPublisherRepository;
use App\Http\Repositories\BookRepository;
use App\Http\Repositories\LibraryRepository;
use Illuminate\Http\Request;
use Validator;

class BookController extends Controller
{
    protected $book;
    protected $request;
    protected $bookAuthor;
    protected $bookCategory;
    protected $bookPublisher;

    public function __construct(
        Request $request,
        BookRepository $book,
        BookCategoryRepository $bookCategory,
        BookAuthorRepository $bookAuthor,
        BookPublisherRepository $bookPublisher,
        LibraryRepository $libraryRepository
    )
    {
        $this->book = $book;
        $this->request = $request;
        $this->bookAuthor = $bookAuthor;
        $this->bookCategory = $bookCategory;
        $this->bookPublisher = $bookPublisher;
        $this->library = $libraryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        return view('book.index-dt', compact('breadcrumb'));
    }

    public function datatable()
    {
        return $books = $this->book->listDatatable();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $categories = $this->bookCategory->list();
        $authors = $this->bookAuthor->list();
        $publishers = $this->bookPublisher->list();
        $libraries = $this->library->list();
        return view('book.create', compact('categories', 'authors', 'publishers', 'breadcrumb', 'libraries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $request = $this->request;
        $data = $request->all();

        $v = Validator::make($data, [
            'title' => 'required',
            'version' => 'required|numeric',
            'image' => 'image'
        ]);

        if ($v->fails()) return redirect()->back()->withErrors($v);

        $response = $this->book->store($data);
        if ($response) {
            return redirect(route('book-index'))->with('success', MESSAGE_GENERAL_CREATE_SUCCESS);
        } else {
            return redirect()->back()->with('error', MESSAGE_GENERAL_CREATE_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $book = $this->book->getOnebook($id);
        $categories = $this->bookCategory->list();
        $authors = $this->bookAuthor->list();
        $publishers = $this->bookPublisher->list();
        $libraries = $this->library->list();
        return view('book.edit', compact('book', 'categories', 'authors', 'publishers', 'breadcrumb', 'libraries'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $request = $this->request;
        $data = $request->all();

        $v = Validator::make($data, [
            'title' => 'required',
            'version' => 'required|numeric',
            'image' => 'image'
        ]);

        if ($v->fails()) return redirect()->back()->withErrors($v);

        $response = $this->book->update($id, $data);
        if ($response) {
            return back()->with('success', MESSAGE_GENERAL_UPDATE_SUCCESS);
        } else {
            return redirect()->back()->with('error', MESSAGE_GENERAL_UPDATE_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createChapter()
    {
        $request = $this->request;
        $response = $this->book->createChapter($request->all());
        if ($response) {
            if ($request->ajax())
                return response()->json([
                    'status' => STATUS_SUCCESS,
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS,
                    'data' => [
                        'chapter' => $response
                    ]
                ]);
            else
                return back()->with('success', MESSAGE_GENERAL_CREATE_SUCCESS);
        } else {
            if ($request->ajax())
                return response()->json([
                    'status' => STATUS_FAIL,
                    'message' => MESSAGE_GENERAL_CREATE_ERROR,
                    'data' => [

                    ]
                ]);
            else
                return redirect()->back()->with('error', MESSAGE_GENERAL_CREATE_ERROR);
        }
    }

    public function viewChapter($id)
    {
        $chapter = $this->book->viewChapter($id);

        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate([$chapter->book_id]);

        return view('book.view-chapter', compact('chapter', 'breadcrumb'));
    }

    public function createPage($id)
    {

        $chapter = $this->book->viewChapter($id);

        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate([$chapter->book_id, $chapter->id]);

        return view('book.create-page', compact('chapter', 'breadcrumb'));
    }

    public function storePage($id)
    {
        $request = $this->request;
        $response = $this->book->storePage($request->all());
        if ($response)
            return redirect()->back()->with('success', MESSAGE_GENERAL_CREATE_SUCCESS);

        return redirect()->back()->with('error', MESSAGE_GENERAL_CREATE_ERROR);
    }

    public function viewPage($bid, $cid, $pid)
    {
        $page = $this->book->viewPage($pid);

        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate([$page->book_id, $page->book_chapter_id]);

        return view('book.view-page', compact('page', 'breadcrumb'));
    }

    public function updatePage($bid, $cid, $pid)
    {

        $this->book->updatePage($pid, $this->request->all());
        return redirect()->back()->with('success', MESSAGE_GENERAL_UPDATE_SUCCESS);
    }
}
