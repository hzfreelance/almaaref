<?php
/**
 * Created by PhpStorm.
 * User: hassanzeaiter
 * Date: 7/24/18
 * Time: 6:39 AM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class Breadcrumb
{

    public function __construct()
    {

    }

    public function generate($params = [])
    {
        $levels = [];
        $paramsIndex = 0;
        if (config('breadcrumb.' . \Request::route()->getName())) {
            foreach (config('breadcrumb.' . \Request::route()->getName()) as $key => $value) {
                if (gettype($value) == 'string')
                    $levels[] = [
                        'name' => $value,
                        'label' => $key
                    ];
                if (gettype($value) == 'array'){
                    $levels[] = [
                        'name' => $value['name'],
                        'label' => $key,
                        'parameters' => $params[$paramsIndex]
                    ];
                    $paramsIndex++;
                }

            }
        }
        return view('partials.breadcrumb', compact('levels'))->render();
    }

}