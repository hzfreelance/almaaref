<?php

namespace App\Http\Controllers;

use App\Http\Repositories\DashboardRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $dashboard;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DashboardRepository $dashboard)
    {
        $this->middleware('auth');
        $this->dashboard = $dashboard;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        $statsSummary = $this->dashboard->statsSummary();
        return view('home', compact('statsSummary', 'breadcrumb'));
    }

    public function syncDb(Request $r)
    {

        // publishers
        $publishers = \DB::connection('mysql_main')
            ->table('publisher')
            ->get();

        \DB::table('book_publishers')->truncate();

        foreach ($publishers as $publisher) {
            \DB::table('book_publishers')
                ->insert([
                    'id' => $publisher->publisher_id,
                    'name' => $publisher->publisher_name
                ]);
        }

        // authors
        $authors = \DB::connection('mysql_main')
            ->table('authors')
            ->get();

        \DB::table('book_authors')->truncate();

        foreach ($authors as $author) {
            \DB::table('book_authors')
                ->insert([
                    'id' => $author->author_id,
                    'name' => $author->author_name
                ]);
        }

        //categories
        $categories = \DB::connection('mysql_main')
            ->table('book_categories')
            ->get();

        \DB::table('book_categories')->truncate();

        foreach ($categories as $category) {
            \DB::table('book_categories')
                ->insert([
                    'id' => $category->category_id,
                    'name' => $category->category_name,
                    'sort_order' => $category->category_order,
                    'image' => $category->category_image,
                    'status' => $category->category_active
                ]);
        }

        //books
        $books = \DB::connection('mysql_main')
            ->table('book')
            ->get();

        \DB::table('books')->truncate();

        foreach ($books as $book) {
            \DB::table('books')
                ->insert([
                    'id' => $book->id,
                    'author_id' => $book->author_id,
                    'category_id' => $book->category_id,
                    'image' => '',
                    'is_publication' => 0,
                    'language' => $book->language,
                    'published_at' => $book->date,
                    'publisher_id' => $book->publisher_id,
                    'status' => $book->active,
                    'version' => $book->version,
                    'summary' => $book->description,
                    'title' => $book->title
                ]);
        }

        //chapters
        $chapters = \DB::connection('mysql_main')
            ->table('chapter')
            ->get();

        \DB::table('book_chapters')->truncate();

        foreach ($chapters as $chapter) {
            \DB::table('book_chapters')
                ->insert([
                    'id' => $chapter->id,
                    'book_id' => $chapter->book_id,
                    'sort_order' => $chapter->order_chapter,
                    'name' => $chapter->title
                ]);
        }

        //pages

        $pages = \DB::connection('mysql_main')
            ->table('chapter_pages')
            ->get();


        \DB::table('book_chapter_pages')->truncate();

        foreach ($pages as $page) {
            \DB::table('book_chapter_pages')
                ->insert([
                    'id' => $page->page_id,
                    'content' => $page->page_content,
                    'sort_order' => $page->page_order,
                    'book_chapter_id' => $page->chapter_id,
                    'book_id' => 0
                ]);
        }
    }
}
