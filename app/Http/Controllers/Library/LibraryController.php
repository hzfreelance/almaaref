<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\LibraryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LibraryController extends Controller
{
    public function __construct(Request $request, LibraryRepository $library)
    {
        parent::__construct($request);
        $this->library = $library;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->library->list();
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();
        return view('library.index', compact('data', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = $this->request;
        $response = $this->library->store($request->all());
        if($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $request = $this->request;
        $response = $this->library->update($id,$request->all());
        if($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
