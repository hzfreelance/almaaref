<?php

namespace App\Http\Controllers\MiscImage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class MiscImageController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'image' => 'required|image'
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return response()->json(['status' => -1, 'data' => $v->messages()->all()]);
        }

        $upload = $this->uploadImage($data['image']);
        if ($upload) {
            return response(['status' => 1, 'data' => url('/') . '/' . $upload]);
        }
        return response()->json(['status' => -1, 'data' => []]);

    }

    protected function uploadImage($image)
    {
        $path = 'misc_images/';
        $name = str_random(16) . '-' . date('Y-m-d-H-i-s');
        $extension = $image->getClientOriginalExtension();
        $fullName = $name . '.' . $extension;

        if ($image->move($path, $fullName)) {
            return $path . '/' . $fullName;
        }

        return false;

    }
}
