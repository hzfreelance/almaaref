<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\BookPublisherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublisherController extends Controller
{

    protected $request;
    protected $bookPublisher;

    public function __construct(Request $request, BookPublisherRepository $bookPublisher)
    {
        parent::__construct($request);
        $this->request = $request;
        $this->bookPublisher = $bookPublisher;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $data = $this->bookPublisher->list();
        return view('publisher.index', compact('data', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = $this->request;
        $response = $this->bookPublisher->store($request->all());
        if($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $request = $this->request;
        $response = $this->bookPublisher->update($request->all(), $id);
        if($response)
            return response()->json([
                'status' => 1,
                'data' => [
                    'message' => MESSAGE_GENERAL_CREATE_SUCCESS
                ]
            ]);

        return response()->json([
            'status' => -1,
            'data' => [
                'message' => MESSAGE_GENERAL_CREATE_ERROR
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
