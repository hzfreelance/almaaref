<?php

namespace App\Http\Controllers\Section;

use App\Http\Controllers\Breadcrumb;
use App\Http\Repositories\BookRepository;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Repositories\SectionRepository;

use Validator;

class SectionController extends Controller
{

    public function __construct(Request $request, SectionRepository $sectionRepository, BookRepository $bookRepository)
    {
        parent::__construct($request);
        $this->sectionRepository = $sectionRepository;
        $this->request = $request;
        $this->bookRepository = $bookRepository;
    }

    public function index(){
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate();

        $data = $this->sectionRepository->listSections();

        return view('section.index', compact('breadcrumb', 'data'));
    }

    public function store(){
        $data = $this->request->all();
        $response = $this->sectionRepository->store($data);
        if($response)
            return response()->json([
                'status' => STATUS_SUCCESS,
                'message' => MESSAGE_GENERAL_CREATE_SUCCESS,
                'data' => [
                    'section' => $response
                ]
            ]);

        return response()->json([
            'status' => STATUS_FAIL,
            'message' => MESSAGE_GENERAL_CREATE_ERROR,
            'data' => []
        ]);
    }

    public function update($id){
        $data = $this->request->all();
        $response = $this->sectionRepository->update($id, $data);
        if($response)
            return response()->json([
                'status' => STATUS_SUCCESS,
                'message' => MESSAGE_GENERAL_UPDATE_SUCCESS,
                'data' => [
                    'section' => $response
                ]
            ]);

        return response()->json([
            'status' => STATUS_FAIL,
            'message' => MESSAGE_GENERAL_UPDATE_ERROR,
            'data' => []
        ]);
    }

    public function show($id){
        $breadcrumb = new Breadcrumb();
        $breadcrumb = $breadcrumb->generate([$id]);

        $books = $this->bookRepository->ApiGetBooks([
            'limit' => 10000
        ]);

        $section = $this->sectionRepository->getSection($id);

        return view('section.show', compact('breadcrumb', 'books', 'section'));
    }

    public function addBooks($sectionId){
        $request = $this->request->all();

        $v = Validator::make($request, ['booksIds' => 'required']);
        if($v->fails()){
            return $v->messages();
        }

        $this->sectionRepository->addBooks($sectionId,$request['booksIds']);

        return response()->json([
            'status' => STATUS_SUCCESS
        ]);

    }
}
