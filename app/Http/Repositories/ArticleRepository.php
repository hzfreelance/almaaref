<?php
/**
 * Created by PhpStorm.
 * User: hassanzeaiter
 * Date: 8/28/18
 * Time: 6:17 PM
 */

namespace App\Http\Repositories;


use App\Models\Article;

class ArticleRepository
{
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function list($pagination = true)
    {
        if ($pagination) {
            return $this->article->newQuery()
                ->with('library')
                ->paginate(10);
        } else {
            return $this->article->newQuery()
                ->with('library')
                ->get();
        }

    }

    public function ApiGetArticles($data)
    {
        $articles = $this->article->newQuery();

        if(isset($data['category_id']))
            $articles = $articles->where('category_id', $data['category_id']);

        $articles = $articles->with('library')
            ->get();

        return $articles;
    }

    public function apiGetArticle($id){
        return $this->article->newQuery()->find($id);
    }

    public function one($id)
    {
        return $this->article->newQuery()->find($id);
    }

    public function store($data)
    {
        $response = $this->article->fill($data);
        if ($response->save())
            return true;
        return false;
    }

    public function update($id, $data)
    {
        $response = $this->article->newQuery()->find($id);
        $response->fill($data);
        if ($response->save())
            return true;
        return false;
    }
}