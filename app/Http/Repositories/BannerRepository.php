<?php
/**
 * Created by PhpStorm.
 * User: hassanzeaiter
 * Date: 8/25/18
 * Time: 5:09 PM
 */

namespace App\Http\Repositories;


use App\Models\Banner;
use App\Models\Book;

class BannerRepository
{
    public function __construct(Banner $banner, Book $book)
    {
        $this->banner = $banner;
        $this->book = $book;
    }

    public function getBanners(){
        return $this->banner->newQuery()->get();
    }

    /**
     * @param $data
     * @return bool
     */
    public function store($data)
    {
        if (isset($data['selected-book'])) {

            $book = $this->book->newQuery()->find($data['selected-book']);
            $data['book_id'] = $book->id;
            $data['title'] = $book->title;
            $data['summary'] = $book->summary;
            $data['image'] = $book->image;

            $query = $this->banner;
            $query->fill($data);

            if ($query->save()) {
                return true;
            }
            return false;

        }

        return false;
    }

    public function update($id, $data)
    {
        $banner = $this->banner->newQuery()->find($id);
        if (isset($data['image']) && $data['image'] !== null) {
            if (!$data['image'] = $this->uploadImage($data['image'])) {
                return false;
            }
        } else {
            $data['image'] = $banner->image;
        }
        $banner->fill($data);
        if ($banner->save()) {
            return true;
        }
        return false;
    }

    /**
     * @param $image
     * @return bool|string
     */
    protected function uploadImage($image)
    {
        $path = 'images/banners';
        $name = str_random(16) . '-' . date('Y-m-d-H-i-s');
        $extension = $image->getClientOriginalExtension();
        $fullName = $name . '.' . $extension;

        if ($image->move($path, $fullName)) {
            return $path . '/' . $fullName;
        }

        return false;

    }

    public function getLatestBanner(){
        return $this->banner->newQuery()->orderby('id','DESC')->first();
    }

}