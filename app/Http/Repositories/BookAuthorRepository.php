<?php
/**
 * Created by PhpStorm.
 * Author: hassa
 * Date: 7/20/2018
 * Time: 12:12 AM
 */

namespace App\Http\Repositories;

use App\Models\BookAuthor;

class BookAuthorRepository
{
    protected $bookAuthor;

    public function __construct(BookAuthor $bookAuthor)
    {
        $this->bookAuthor = $bookAuthor;
    }

    public function list()
    {
        $query = $this->bookAuthor->newQuery();
        $query = $query->paginate(10);

        return $query;
    }

    public function store($data){
        $query = $this->bookAuthor->fill($data);
        $query->save();
        if($query)
            return true;

        return false;
    }

    public function update($data,$id){
        $query = $this->bookAuthor->newQuery()->find($id);
        $query->fill($data);
        $query->save();
        if($query)
            return true;

        return false;
    }

    public function countAuthor(){
        return $this->bookAuthor->newQuery()->count();
    }

}