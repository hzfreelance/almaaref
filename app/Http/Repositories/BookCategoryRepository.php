<?php
/**
 * Created by PhpStorm.
 * Author: hassa
 * Date: 7/20/2018
 * Time: 12:09 AM
 */

namespace App\Http\Repositories;

use App\Models\Book;
use App\Models\BookCategory;

class BookCategoryRepository
{

    protected $bookCategory;
    protected $book;

    public function __construct(BookCategory $bookCategory, Book $book)
    {
        $this->bookCategory = $bookCategory;
        $this->book = $book;
    }

    public function list($paginate = true)
    {

        $query = $this->bookCategory->newQuery();
        if ($paginate)
            $query = $query->paginate(10);
        else
            $query = $query->get();

        return $query;
    }

    public function store($data)
    {
        if (isset($data['image']))
            $data['image'] = $this->uploadImage($data['image']);
        else
            $data['image'] = null;

        $query = $this->bookCategory;
        if ($data['library_id'] == 1) {
            $data['parent_id'] = 0;
        }
        $query->fill($data);

        if ($query->save()) {
            return true;
        }
        return false;
    }

    public function getOne($id)
    {
        return $this->bookCategory->newQuery()->find($id);
    }

    public function update($data, $id)
    {
        $category = $this->bookCategory->newQuery()->find($id);
        if (isset($data['image']) && $data['image'] !== null) {
            if (!$data['image'] = $this->uploadImage($data['image'])) {
                return false;
            }
            if (isset($category->image))
                unlink($category->image);
        } else {
            $data['image'] = $category->image;
        }
        if ($data['library_id'] == 1) {
            $data['parent_id'] = 0;
        }
        $category->fill($data);
        if ($category->save()) {
            return true;
        }
        return false;
    }

    protected function uploadImage($image)
    {
        $path = 'images/categories';
        $name = str_random(16) . '-' . date('Y-m-d-H-i-s');
        $extension = $image->getClientOriginalExtension();
        $fullName = $name . '.' . $extension;

        if ($image->move($path, $fullName)) {
            return $path . '/' . $fullName;
        }

        return false;

    }

    public function countCategory($status)
    {
        return $this->bookCategory->newQuery()->where('status', $status)->count();
    }

    public function ApiGetBooksByCategories($data)
    {

        $return = [];

        $categories = $this->bookCategory->newQuery()
            ->where('status', 1)
            //->whereHas('books')
            ->get();

        foreach ($categories as $category) {

            $books = $this->book->newQuery();
            $books = $books->where('category_id', $category->id);
            $books = $books->limit(9)->get();

            $category['books'] = $books;

            $return[] = $category;

        }

        return $return;

    }

    public function ApiGetCategoryWithBooks($id, $data)
    {

        $limit = 9;
        $offset = 0;

        if (isset($data['limit']))
            $limit = $data['limit'];

        if (isset($data['offset']))
            $offset = $data['offset'];

        return $this->bookCategory->newQuery()
            ->where('status', 1)
            ->with(['books' => function ($q) use ($limit, $offset) {
                $q->take($limit)->skip($offset);
            }])
            ->whereHas('books')
            ->find($id);
    }

    public function getCategoriesLevels($onlyPublished = false)
    {
        $query = $this->bookCategory->newQuery()
            ->where('parent_id', 0)
            ->where('library_id', 2);

        if ($onlyPublished)
            $query = $query->where('status', 1);

        $query = $query->get();

        return $query;
    }

}