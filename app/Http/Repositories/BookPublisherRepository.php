<?php
/**
 * Created by PhpStorm.
 * Author: hassa
 * Date: 7/20/2018
 * Time: 12:13 AM
 */

namespace App\Http\Repositories;


use App\Models\BookPublisher;

class BookPublisherRepository
{
    protected $bookPublisher;

    public function __construct(BookPublisher $bookPublisher)
    {
        $this->bookPublisher = $bookPublisher;
    }

    public function list()
    {
        $query = $this->bookPublisher->newQuery();
        $query = $query->paginate(10);

        return $query;
    }

    public function store($data){
        $query = $this->bookPublisher->fill($data);
        $query->save();
        if($query)
            return true;

        return false;
    }

    public function update($data,$id){
        $query = $this->bookPublisher->newQuery()->find($id);
        $query->fill($data);
        $query->save();
        if($query)
            return true;

        return false;
    }

    public function countPublisher(){
        return $this->bookPublisher->newQuery()->count();
    }
}