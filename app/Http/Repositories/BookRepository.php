<?php
/**
 * Created by PhpStorm.
 * Author: hassa
 * Date: 7/19/2018
 * Time: 11:50 PM
 */

namespace App\Http\Repositories;

use App\Models\Book;
use App\Models\BookChapter;
use App\Models\BookChapterPage;
use Yajra\DataTables\DataTables;
use Builder;


class BookRepository
{

    /**
     * @var Book
     */
    protected $book;
    /**
     * @var DataTables
     */
    protected $datatables;

    /**
     * @var BookChapter
     */
    protected $bookChapter;

    protected $bookChapterPage;

    /**
     * BookRepository constructor.
     * @param Book $book
     */
    public function __construct(Book $book, DataTables $dataTables, BookChapter $bookChapter, BookChapterPage $bookChapterPage)
    {
        $this->book = $book;
        $this->datatables = $dataTables;
        $this->bookChapter = $bookChapter;
        $this->bookChapterPage = $bookChapterPage;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listPaginated()
    {
        $query = $this->book->newQuery();
        $query->with(['category', 'author', 'publisher', 'library']);
        return $query->paginate(10);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listDatatable()
    {
        $query = $this->book->newQuery();
        $query->with(['category', 'author', 'publisher', 'library'])->get();

        return $this->datatables->eloquent($query)
            ->addColumn('action', 'eloquent.tables.users-action')
            ->make(true);
    }

    public function list(){
        return $this->book->get(['id','title']);
    }

    /**
     * @param $data
     * @return bool
     */
    public function store($data)
    {
        if (isset($data['image']))
            $data['image'] = $this->uploadImage($data['image']);
        else
            $data['image'] = null;
        $query = $this->book;
        $query->fill($data);

        if ($query->save()) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data)
    {
        $book = $this->book->newQuery()->find($id);
        if (isset($data['image']) && $data['image'] != '') {
            if (!$data['image'] = $this->uploadImage($data['image'])) {
                return false;
            }
//            if (isset($book->image))
//                unlink($book->image);
        } else {
            //$data['image'] = $book->image;
        }
        $book->fill($data);
        if ($book->save()) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getOnebook($id)
    {
        return $query = $this->book->newQuery()->find($id);
    }

    /**
     * @param $image
     * @return bool|string
     */
    protected function uploadImage($image)
    {
        $path = 'images/books';
        $name = str_random(16) . '-' . date('Y-m-d-H-i-s');
        $extension = $image->getClientOriginalExtension();
        $fullName = $name . '.' . $extension;

        if ($image->move($path, $fullName)) {
            return $path . '/' . $fullName;
        }

        return false;

    }

    /**
     * @param $status
     * @return int
     */
    public function countBook($status)
    {
        return $this->book->newQuery()->where('status', $status)->count();
    }

    /**
     * @param $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function ApiGetBooks($data)
    {
        $limit = 9;
        $offset = 0;

        $response = $this->book->newQuery();

        if (isset($data['limit']))
            $limit = $data['limit'];

        if (isset($data['offset']))
            $offset = $data['offset'];

        if(isset($data['library_id']))
            $response = $response->where('library_id', $data['library_id']);
        else
            $response = $response->where('library_id', 1);

        $response = $response->take($limit);
        $response = $response->skip($offset);
        $response = $response->where('status', 1);
        $response = $response->where('is_publication', 0);
        $response = $response->orderby('id', 'DESC');

        return $response->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function ApiFindABook($data)
    {
        $limit = 9;
        $offset = 0;

        $response = $this->book->newQuery();

        if (isset($data['limit']))
            $limit = $data['limit'];

        if (isset($data['offset']))
            $offset = $data['offset'];

        if (isset($data['category']))
            $response = $response->whereHas('category', function ($q) use ($data) {
                $q->where('category_id', $data['category']);
            });

        if (isset($data['author']))
            $response = $response->whereHas('author', function ($q) use ($data) {
                $q->where('author_id', $data['author']);
            });

        if (isset($data['term']))
            $response = $response->where('title', 'LIKE', '%' . $data['term'] . '%');

        $response = $response->take($limit);
        $response = $response->skip($offset);
        $response = $response->where('status', 1);
        $response = $response->where('is_publication', 0);
        $response = $response->orderby('id', 'DESC');

        return $response->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function ApiGetBook($id)
    {
        return $this->book->newQuery()->where('status', 1)->where('is_publication', 0)->find($id);
    }

    /**
     * @param $data
     * @return bool
     */
    public function createChapter($data)
    {
        $response = $this->bookChapter;
        if ($response->fill($data)->save())
            return true;
        return false;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function viewChapter($id)
    {
        return $this->bookChapter->newQuery()->find($id);
    }

    public function storePage($data){
        return $this->bookChapterPage->fill($data)->save();
    }

    public function viewPage($id){
        return $this->bookChapterPage->newQuery()->find($id);
    }

    public function updatePage($id,$data){
        $page = $this->bookChapterPage->newQuery()->find($id);
        return $page->fill($data)->save();
    }

    public function getBookDetails($id){
        $book = $this->book->newQuery();
        $book = $book->with(['chapters' => function($q){
            $q->with('pages');
        }]);
        return $book->find($id);
    }

}