<?php
/**
 * Created by PhpStorm.
 * User: hassanzeaiter
 * Date: 7/22/18
 * Time: 9:33 PM
 */

namespace App\Http\Repositories;


class DashboardRepository
{

    protected $book;
    protected $bookAuthor;
    protected $bookCategory;
    protected $bookPublisher;

    public function __construct(BookRepository $book, BookAuthorRepository $bookAuthor, BookCategoryRepository $bookCategory, BookPublisherRepository $bookPublisher)
    {
        $this->book = $book;
        $this->bookAuthor = $bookAuthor;
        $this->bookCategory = $bookCategory;
        $this->bookPublisher = $bookPublisher;
    }

    public function statsSummary(){
        $countBookPublished = $this->book->countBook(1);
        $countBookUnPublished = $this->book->countBook(0);
        $countAuthor = $this->bookAuthor->countAuthor();
        $countPublisher = $this->bookPublisher->countPublisher();
        $countCategoryPublished = $this->bookCategory->countCategory(1);
        $countCategoryUnPublished = $this->bookCategory->countCategory(0);

        return [
            'book_published' => $countBookPublished,
            'book_unpublished' => $countBookUnPublished,
            'author' => $countAuthor,
            'publisher' => $countPublisher,
            'category_published' => $countCategoryPublished,
            'category_unpublished' => $countCategoryUnPublished
        ];

    }

}