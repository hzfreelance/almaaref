<?php
/**
 * Created by PhpStorm.
 * User: hassanzeaiter
 * Date: 8/28/18
 * Time: 6:00 PM
 */

namespace App\Http\Repositories;


use App\Models\Library;

class LibraryRepository
{
    public function __construct(Library $library)
    {
        $this->library = $library;
    }

    public function list()
    {
        return $this->library->newQuery()->get();
    }

    public function store($data)
    {
        $response = $this->library->fill($data);
        if ($response->save())
            return true;
        return false;
    }

    public function update($id, $data)
    {
        $response = $this->library->newQuery()->find($id);
        $response->fill($data);
        if ($response->save())
            return true;
        return false;
    }
}