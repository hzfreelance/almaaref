<?php
/**
 * Created by PhpStorm.
 * Author: hassa
 * Date: 7/25/2018
 * Time: 9:34 PM
 */

namespace App\Http\Repositories;


use App\Models\Section;

class SectionRepository
{

    protected $section;

    public function __construct(Section $section)
    {
        $this->section = $section;
    }

    public function listSections()
    {
        $query = $this->section->newQuery();
        $query->with('books');
        return $query->get();
    }

    public function store($data)
    {
        return $this->section->fill($data)->save();
    }

    public function update($id, $data)
    {
        $row = $this->section->newQuery()->find($id);
        return $row = $row->fill($data)->save();
    }

    public function addBooks($sectionId, $booksIds)
    {
        $section = $this->section->newQuery()->find($sectionId);
        return $section->books()->sync($booksIds);
    }

    public function getSection($id){
        return $this->section->newQuery()->find($id);
    }

}