<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['title', 'image', 'sort_order', 'book_id', 'summary'];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

}
