<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $casts = ['version' => 'integer'];
    protected $fillable = [
        'title',
        'summary',
        'image',
        'version',
        'status',
        'published_at',
        'language',
        'category_id',
        'author_id',
        'publisher_id',
        'library_id'
    ];

    public function category()
    {
        return $this->belongsTo(BookCategory::class, 'category_id');
    }

    public function author()
    {
        return $this->belongsTo(BookAuthor::class, 'author_id');
    }

    public function publisher()
    {
        return $this->belongsTo(BookPublisher::class, 'publisher_id');
    }

    public function getImageAttribute($value)
    {
        if($value)
            return url('/') . '/' . $value;
        return null;
    }

    public function chapters()
    {
        return $this->hasMany(BookChapter::class);
    }

    public function pages()
    {
        return $this->hasMany(BookChapterPage::class);
    }

    public function library()
    {
        return $this->belongsTo(Library::class);
    }

}
