<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookChapter extends Model
{

    protected $fillable = ['name','book_id','sort_order'];

    public function pages(){
        return $this->hasMany(BookChapterPage::class);
    }
}
