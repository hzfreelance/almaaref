<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookChapterPage extends Model
{
    protected $fillable = ['book_id','book_chapter_id','content','sort_order'];
}
