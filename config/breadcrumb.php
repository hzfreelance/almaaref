<?php
/**
 * Created by PhpStorm.
 * Author: Hassan Zeaiter
 * Date: 7/19/2018
 * Time: 10:50 PM
 */

return [
    'book-index' => [
        'لائحة الكتب' => 'book-index',
    ],
    'book-create' => [
        'لائحة الكتب' => 'book-index',
        'كتاب جديد' => 'book-create'
    ],
    'book-edit' => [
        'لائحة الكتب' => 'book-index',
        'تعديل كتاب' => 'book-edit'
    ],
    'category-index' => [
        'لائحة الفئات' => 'category-index'
    ],
    'category-edit' => [
        'لائحة الفئات' => 'category-index',
        'تعديل فئة' => 'category-edit'
    ],
    'author-index' => [
        'لائحة الكتاب' => 'author-index'
    ],
    'publisher-index' => [
        'لائحة دور النشر' => 'publisher-index'
    ],
    'home' => [
        'الرئيسية' => 'home'
    ],
    'book-view-chapter' => [
        'لائحة الكتب' => 'book-index',
        'تعديل كتاب' => [
            'name' => 'book-edit',
            'has_parameter' => 1
        ],
        'قسم من كتاب' => 'book-view-chapter'
    ],
    'book-create-page' => [
        'لائحة الكتب' => 'book-index',
        'تعديل كتاب' => [
            'name' => 'book-edit',
            'has_parameters' => 1
        ],
        'قسم من كتاب' => [
            'name' => 'book-view-chapter',
            'has_parameters' => 1
        ],
        'صفحة جديدة' => 'book-create-page'
    ],
    'book-view-page' => [
        'لائحة الكتب' => 'book-index',
        'تعديل كتاب' => [
            'name' => 'book-edit',
            'has_parameters' => 1
        ],
        'قسم من كتاب' => [
            'name' => 'book-view-chapter',
            'has_parameters' => 1
        ],
        'تعديل صفحة' => 'book-view-page'
    ],
    'section-index' => [
        'لائحة الاقسام' => 'section-index'
    ],
    'section-show' => [
        'لائحة الاقسام' => 'section-index',
        'مشاهدة قسم' => 'section-show'
    ],
    'banner-index' => [
        'الاعلانات' => 'banner-index'
    ],
    'library-index' => [
        'المكتبات' => 'library-index'
    ],
    'article-index' => [
        'المقالات' => 'article-index'
    ],
    'article-create' => [
        'المقالات' => 'article-index',
        'مقال جديد' => 'article-create'
    ],
    'article-edit' => [
        'المقالات' => 'article-index',
        'تعديل مقال' => 'article-edit'
    ],
    'article-category-index' => [
        'لائحة الفئات' => 'article-category-index'
    ],
    'article-category-edit' => [
        'لائحة الفئات' => 'article-category-index',
        'تعديل فئة' => 'article-category-edit'
    ],
];