<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToMultipleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_categories', function (Blueprint $table) {
            $table->datetime('deleted_at')->after('sort_order')->nullable();
        });

        Schema::table('book_authors', function (Blueprint $table) {
            $table->datetime('deleted_at')->after('name')->nullable();
        });

        Schema::table('book_publishers', function (Blueprint $table) {
            $table->datetime('deleted_at')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_categories', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('book_authors', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('book_publishers', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
