<?php

use Illuminate\Database\Seeder;

class DummyDateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book_categories')->insert([
            'name' => 'واحد'
        ]);
        DB::table('book_categories')->insert([
            'name' => 'اثنان'
        ]);
        DB::table('book_categories')->insert([
            'name' => 'ثلاثة'
        ]);
        DB::table('book_categories')->insert([
            'name' => 'اربعة'
        ]);
        DB::table('book_categories')->insert([
            'name' => 'خمسة'
        ]);

        DB::table('book_authors')->insert([
            'name' => 'واحد'
        ]);
        DB::table('book_authors')->insert([
            'name' => 'اثنان'
        ]);
        DB::table('book_authors')->insert([
            'name' => 'ثلاثة'
        ]);
        DB::table('book_authors')->insert([
            'name' => 'اربعة'
        ]);
        DB::table('book_authors')->insert([
            'name' => 'خمسة'
        ]);

        DB::table('book_publishers')->insert([
            'name' => 'واحد'
        ]);
        DB::table('book_publishers')->insert([
            'name' => 'اثنان'
        ]);
        DB::table('book_publishers')->insert([
            'name' => 'ثلاثة'
        ]);
        DB::table('book_publishers')->insert([
            'name' => 'اربعة'
        ]);
        DB::table('book_publishers')->insert([
            'name' => 'خمسة'
        ]);
    }
}
