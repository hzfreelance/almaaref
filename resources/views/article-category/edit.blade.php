@extends('layouts.app')

@section('content')
    <form method="POST" action="{{route('article-category-update', $category->id)}}" enctype="multipart/form-data"
          id="form-update">
        <div class="row">

            @csrf
            @method('put')
            <div class="col-md-12">
                <p>
                    <label for="name">الاسم</label>
                    <input type="text" class="form-control" placeholder="الاسم" name="name" value="{{$category->name}}">
                </p>
            </div>
            {{--<div class="col-md-4">--}}
                {{--<div style="background: url('{{url('/').'/'.$category->image}}'); background-size: contain; background-repeat: no-repeat; height: 200px; background-position: center">--}}

                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-8">--}}
                {{--<p>--}}
                    {{--<label for="image">الصورة</label>--}}
                    {{--<input type="file" id="image" name="image" class="form-control">--}}
                {{--</p>--}}
            {{--</div>--}}
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <p>
                    <label for="parent_id">تابع لفئة أخرى</label>
                    <select id="parnet_id" name="parent_id" class="form-control">
                        <option value="0" {{($category->parent_id == 0) ? 'selected' : ''}}>غير تابع</option>
                        @foreach($categories as $categoryp)
                            @if($categoryp->id != $category->id)
                                <option value="{{$categoryp->id}}" {{($categoryp->id == $category->parent_id) ? 'selected' : ''}}>{{$categoryp->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="status">الحالة</label>
                    <select name="status" class="form-control">
                        <option value="1" {{ ($category->status == 1) ? 'selected' : ''  }}>منشور</option>
                        <option value="0" {{ ($category->status == 0) ? 'selected' : ''  }}>غير منشور</option>
                    </select>
                </p>
            </div>
            <div class="col-md-4 offset-8 text-left">
                <button type="submit" class="btn btn-success form-control">حفظ</button>
            </div>

        </div>
    </form>
@endsection