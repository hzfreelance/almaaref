@extends('layouts.app')

@section('content')

    <form method="POST" action="{{route('article-update', $article->id)}}">
        @csrf
        @method('put')
        <div class="row">
            <div class="col-md-12">
                <p>
                    <label for="title">العنوان</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$article->title}}">
                </p>
                <input type="hidden" name="library_id" value="3">
                <p>
                    <label for="category_id">الفئة</label>
                    <select class="form-control" name="category_id">
                        @foreach($categories as $category)
                            @if(count($category->children) == 0)
                                <option value="{{$category->id}}" {{($category->id == $article->category_id) ? 'selected' : ''}}>
                                    @if(isset($category->parent->parent->parent->parent->parent->parent->name))
                                        {{$category->parent->parent->parent->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($category->parent->parent->parent->parent->parent->name))
                                        {{$category->parent->parent->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($category->parent->parent->parent->parent->name))
                                        {{$category->parent->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($category->parent->parent->parent->name))
                                        {{$category->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($category->parent->parent->name))
                                        {{$category->parent->parent->name}} ->
                                    @endif
                                    @if(isset($category->parent->name))
                                        {{$category->parent->name}} ->
                                    @endif
                                    {{$category->name}}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </p>
                <p>
                    <label for="content">المحتوى</label>
                    <textarea id="content" name="content">{{$article->content}}</textarea>
                </p>
                <p>
                    <input type="submit" value="حفظ" class="form-control btn btn-success">
                </p>
            </div>
        </div>
    </form>

@endsection


@section('script')

    <script>
        CKEDITOR.replace('content', {
            language: 'ar',
            height: 500
        });
    </script>

@endsection