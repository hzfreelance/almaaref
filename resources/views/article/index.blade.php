@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if(count($data) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>العنوان</th>
                        <th>المكتبة</th>
                        <th>الفئة</th>
                        <th>التاريخ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $article)
                        <tr>
                            <td>
                                {{$article->title}}
                            </td>
                            <td>
                                {{$article->library->name}}
                            </td>
                            <td>
                                {{$article->category->name}}
                            </td>
                            <td>
                                {{$article->created_at}}
                            </td>
                            <td>
                                <a href="{{route('article-edit', $article->id)}}">مشاهدة</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $data->render() !!}
            @else
                <div class="alert alert-warning">لا يوجد نتائج، <a href="{{route('article-create')}}"> انشاء جديد</a></div>

            @endif
        </div>
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="{{route('article-create')}}"><i class="far fa-edit fa-4x"></i></a>
    </div>


@endsection

@section('script')


@endsection