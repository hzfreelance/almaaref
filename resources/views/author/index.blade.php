@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if(count($data) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>لاسم</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td>{{$row->name}}</td>
                            <td><a href="#" data-toggle="modal" data-target="#modal-update" data-row="{{$row}}">مشاهدة</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $data->render() !!}
            @else
                <div class="alert alert-warning">لا يوجد نتائج، <a data-toggle="modal" data-target="#modal-create" href="#"> انشاء جديد</a></div>

            @endif
        </div>
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="#" data-toggle="modal" data-target="#modal-create"><i class="far fa-edit fa-4x"></i></a>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-create">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">كاتب جديد</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('author-store')}}" enctype="multipart/form-data" id="form-create">
                        @csrf
                        <p>
                            <label for="name">الاسم</label>
                            <input type="text" class="form-control" placeholder="الاسم" name="name">
                        </p>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-update">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="#" enctype="multipart/form-data" id="form-update">
                        @csrf
                        @method('put')
                        <p>
                            <label for="name">الاسم</label>
                            <input type="text" class="form-control" placeholder="الاسم" name="name">
                        </p>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $('#form-create').submit(function(e){

            e.preventDefault();

            $.ajax({
                url: '{{route('author-store')}}',
                type: 'POST',
                data: $(this).serialize(),
                cache: false,
                datatype: 'JSON',
                success: function(data){
                    if(data.status == 1){
                        location.reload();
                    }
                },error: function(){

                }
            });

        });

        $("#form-update").submit(function(e){
           e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                type: 'PUT',
                data: $(this).serialize(),
                cache: false,
                datatype: 'JSON',
                success: function(data){
                    if(data.status == 1){
                        location.reload();
                    }
                },error: function(){

                }
            });

        });

        $('#modal-update').on('show.bs.modal', function (event) {
            var anchor = $(event.relatedTarget) // Button that triggered the modal

            var recipient = anchor.data('row') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text(' تعديل ' + recipient.name)
            modal.find('.modal-body input[type=text]').val(recipient.name)
            modal.find('.modal-body form').attr('action', '{{url('/')}}/author/'+recipient.id);
        })

    </script>



@endsection