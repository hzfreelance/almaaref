@extends('layouts.app')

@section('content')

    <div class="row">
        @if(count($data) > 0)
            @foreach($data as $row)
                {{--<div class="col-md-4" data-toggle="modal" data-target="#modal-update" data-row="{{$row}}"--}}
                     {{--style="margin-bottom: 5px">--}}
                <div class="col-md-4" style="margin-bottom: 5px">
                    <div style="height: 200px;width: 100%;background: url({{$row->image}});background-position: center;background-size: cover"></div>
                    <h4 class="text-center text" style="padding: 5px">
                        {{$row->title}}
                    </h4>
                </div>
            @endforeach
        @else
            <div class="col-md-12 alert alert-warning">لا يوجد نتائج، <a data-toggle="modal" data-target="#modal-create"
                                                                         href="#"> انشاء جديد</a></div>
        @endif
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="#" data-toggle="modal" data-target="#modal-create" data-backdrop="static"><i class="far fa-edit fa-4x"></i></a>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-create">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">اعلان جديد</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('banner-store')}}" enctype="multipart/form-data"
                          id="form-create">
                        @csrf
                        <p>
                            <label for="selected-book">ابحث عن كتاب</label>
                            <select name="selected-book" id="selected-book">
                                <option></option>
                                @foreach($booksList as $book)
                                    <option value="{{$book->id}}">{{$book->title}}</option>
                                @endforeach
                            </select>
                        </p>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-update">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="#" enctype="multipart/form-data" id="form-update">
                        @csrf
                        @method('put')
                        <p>
                            <label for="image">الصورة</label>
                            <input id="image" type="file" class="form-control" placeholder="الصورة" name="image">
                        </p>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>

        $('#form-create').submit(function (e) {

            e.preventDefault();

            $.ajax({
                url: '{{route('banner-store')}}',
                type: 'POST',
                data: new FormData(this),
                cache: false,
                datatype: 'JSON',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.status == 1) {
                        location.reload();
                    }
                }, error: function () {

                }
            });

        });

        $("#form-update").submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                cache: false,
                datatype: 'JSON',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.status == 1) {
                        location.reload();
                    }
                }, error: function () {

                }
            });

        });

        $('#modal-update').on('show.bs.modal', function (event) {
            var anchor = $(event.relatedTarget) // Button that triggered the modal

            var recipient = anchor.data('row') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text(' تعديل ')
            //modal.find('.modal-body input[type=text]').val(recipient.name)
            modal.find('.modal-body form').attr('action', '{{url('/')}}/banner/' + recipient.id);
        })

        $('#selected-book').selectize({
            placeholder: 'ابحث'
        });

    </script>



@endsection