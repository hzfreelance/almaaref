@extends('layouts.app')

@section('content')

    <form method="POST" action="{{route('book-store-page', $chapter->id)}}">
        @csrf
        <div class="row">
            <div class="col-md-12">

                <p>
                    <label for="sort_order">الترتيب</label>
                    <input type="number" class="form-control" name="sort_order" id="sort_order">
                </p>

                <p>
                    <label for="content">محتوى الصفحة كما يجب ان يظهر</label>
                    <textarea class="form-control" id="content" name="content"></textarea>
                </p>

                <input type="hidden" name="book_id" value="{{$chapter->book_id}}">
                <input type="hidden" name="book_chapter_id" value="{{$chapter->id}}">

            </div>
            <div class="col-md-4 offset-8">
                <input type="submit" value="حفظ" class="form-control btn btn-primary">
            </div>
        </div>
    </form>

    <hr>

    <form method="POST" action="{{route('misc-images.store')}}" enctype="multipart/form-data" id="upload-image">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <input type="file" name="image" id="image">
            </div>
            <div class="col-md-4">
                <button class="btn btn-success form-control">تحميل الصورة</button>
            </div>
        </div>
    </form>

    <div id="results"></div>

    <br>
    <br>

@endsection

@section('script')

    <script>
        CKEDITOR.replace('content', {
            language: 'ar',
            height: 500
        });

        $('#upload-image').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route('misc-images.store')}}',
                type: 'POST',
                data: new FormData(this),
                cache: false,
                datatype: 'JSON',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.status == 1) {
                        $('#image').val('');
                        $('#results').append(data.data+'<br>');
                    }
                }, error: function () {

                }
            });
        });

    </script>

@endsection