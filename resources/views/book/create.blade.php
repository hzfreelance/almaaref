@extends('layouts.app')

@section('content')
    <form method="POST" action="{{route('book-store')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <p>
                    <label for="title">العنوان</label>
                    <input type="text" class="form-control" id="title" name="title">
                </p>
            </div>
            <div class="col-md-12">
                <p>
                    <label for="summary">وصف مصغر</label>
                    <textarea class="form-control" id="summary" name="summary"
                              style="height: 150px; resize: none"></textarea>
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="image">الصورة</label>
                    <input type="file" class="form-control" id="image" name="image">
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="status">الحالة</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1">منشور</option>
                        <option value="0">غير منشور</option>
                    </select>
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="version">رقم الاصدار</label>
                    <input type="number" class="form-control" id="version" name="version">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="published_at">تاريخ الاصدار</label>
                    <input type="date" class="form-control" id="published_at" name="published_at">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="language">اللغة</label>
                    <input type="text" class="form-control" id="language" name="language">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="library_id">المكتبة</label>
                    <select class="form-control" id="library_id" name="library_id">
                        @foreach($libraries as $library)
                            @if($library->id != 3)
                            <option value="{{$library->id}}">{{$library->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="category">الفئة</label>
                    <select class="form-control" id="category" name="category_id">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="author">الكاتب</label>
                    <select class="form-control" id="author" name="author_id">
                        @foreach($authors as $author)
                            <option value="{{$author->id}}">{{$author->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="publisher">دار النشر</label>
                    <select class="form-control" id="publisher" name="publisher_id">
                        @foreach($publishers as $publisher)
                            <option value="{{$publisher->id}}">{{$publisher->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <p>
                    <input type="submit" class="form-control btn btn-primary" value="حفظ">
                </p>
            </div>
        </div>
    </form>
@endsection