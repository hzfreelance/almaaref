@extends('layouts.app')

@section('content')
    <form method="POST" action="{{route('book-update', $book->id)}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="row">
            <div class="col-md-12">
                <p>
                    <label for="title">العنوان</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$book->title}}">
                </p>
            </div>
            <div class="col-md-12">
                <p>
                    <label for="summary">وصف مصغر</label>
                    <textarea class="form-control" id="summary" name="summary"
                              style="height: 150px; resize: none">{{$book->summary}}</textarea>
                </p>
            </div>
            <div class="col-md-2">
                @if($book->image)
                    <img src="{{asset($book->image)}}" style="width: 100%">
                @else
                    لا يوجد صورة
                @endif
            </div>
            <div class="col-md-4">
                <p>
                    <label for="image">الصورة</label>
                    <input type="file" class="form-control" id="image" name="image">
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="status">الحالة</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1" {{($book->status == 1) ? 'selected' : ''}}>منشور</option>
                        <option value="0" {{($book->status == 0) ? 'selected' : ''}}>غير منشور</option>
                    </select>
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <p>
                    <label for="version">رقم الاصدار</label>
                    <input type="number" class="form-control" id="version" name="version" value="{{$book->version}}">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="published_at">تاريخ الاصدار</label>
                    <input type="date" class="form-control" id="published_at" name="published_at"
                           value="{{$book->published_at}}">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="language">اللغة</label>
                    <input type="text" class="form-control" id="language" name="language" value="{{$book->language}}">
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    <label for="library_id">المكتبة</label>
                    <select class="form-control" id="library_id" name="library_id">
                        @foreach($libraries as $library)
                            @if($library->id != 3)
                                <option {{($library->id == $book->library_id) ? 'selected' : ''}} value="{{$library->id}}">{{$library->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="category">الفئة</label>
                    <select class="form-control" id="category" name="category_id">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" {{($book->category_id == $category->id) ? 'selected' : ''}}>{{$category->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="author">الكاتب</label>
                    <select class="form-control" id="author" name="author_id">
                        @foreach($authors as $author)
                            <option value="{{$author->id}}" {{($book->author_id == $author->id) ? 'selected' : ''}}>{{$author->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <label for="publisher">دار النشر</label>
                    <select class="form-control" id="publisher" name="publisher_id">
                        @foreach($publishers as $publisher)
                            <option value="{{$publisher->id}}" {{($book->publisher_id == $publisher->id) ? 'selected' : ''}}>{{$publisher->name}}</option>
                        @endforeach
                    </select>
                </p>
            </div>
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <p>
                    <input type="submit" class="form-control btn btn-success" value="حفظ">
                </p>
            </div>
        </div>
    </form>

    <hr>

    <div class="row">
        <div class="col-md-10">
            <h3>أقسام الكتاب</h3>
        </div>
        <div class="col-m-2 text-left">
            <h3><a href="#" data-toggle="modal" data-target="#modal-create">قسم جديد</a></h3>
        </div>
    </div>

    <hr>

    <div class="row">
        @if(count($book->chapters) > 0)
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>الترتيب</th>
                        <th>الاسم</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($book->chapters as $chapter)
                        <tr>
                            <td>{{$chapter->sort_order}}</td>
                            <td>{{$chapter->name}}</td>
                            <td><a href="{{route('book-view-chapter',$chapter->id)}}">مشاهدة</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="col-md-12">
                <div class="alert alert-warning">ان هذا الكتاب لا يحتوي على اي قسم</div>
            </div>
        @endif
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-create">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">قسم جديد</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="#" enctype="multipart/form-data" id="form-create">
                        @csrf
                        <p>
                            <label for="name">الاسم</label>
                            <input type="text" class="form-control" placeholder="الاسم" name="name">
                        </p>
                        <p>
                            <label for="sort_order">الترتيب</label>
                            <input type="number" class="form-control" name="sort_order" placeholder="الترتيب">
                        </p>
                        <input type="hidden" name="book_id" value="{{$book->id}}">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $('#form-create').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '{{route('book-create-chapter')}}',
                type: 'POST',
                data: $(this).serialize(),
                cache: false,
                datatype: 'JSON',
                success: function (data) {
                    if (data.status == 1)
                        location.reload();
                }, error: function () {

                }
            });

        });
    </script>

@endsection