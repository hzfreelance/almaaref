@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="books">
                <thead>
                <tr>
                    <th>العنوان</th>
                    <th>الفئة</th>
                    <th>الكاتب</th>
                    <th>دار النشر</th>
                    <th>المكتبة</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="{{route('book-create')}}"><i class="far fa-edit fa-4x"></i></a>
    </div>

@endsection

@section('script')

    <script>
        $(function () {
            $('#books').DataTable({
                "language": {
                    "lengthMenu": "عرض __MENU__ نتائج في الصفحة",
                    "zeroRecords": "لم يتم العثور على نتائج",
                    "info" : "الصفحة __PAGE__ من __PAGES__",
                    "infoEmpty" : "لا نتائج",
                    "infoFiltered" : "(من اصل __MAX__ نتائج اساسية)",
                    "search" : "البحث : ",
                    "paginate" : {
                        "previous" : "السابق",
                        "next" : "التالي"
                    }
                },
                processing: true,
                serverSide: true,
                ajax: '{!! route('book-datatable') !!}',
                columns: [
                    {data: 'title'},
                    {
                        data: 'category_id',
                        render: function (data, row, item) {
                            return item.category.name;
                        }
                    },
                    {
                        data: 'author_id',
                        render: function (data, row, item) {
                            return item.author.name;
                        }
                    },
                    {
                        data: 'publisher_id',
                        render: function (data, row, item) {
                            return item.publisher.name
                        }
                    },
                    {
                        data: 'library_id',
                        render: function (data, row, item) {
                            return item.library.name
                        }
                    },
                    {
                        data: 'id',
                        render: function (data, row, item) {
                            return '<a href="{{url('/')}}/book/' + item.id + '/edit">مشاهدة</a>';
                        }
                    }
                ]
            });
        });
    </script>

@endsection