@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if(count($books) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>العنوان</th>
                        <th>الفئة</th>
                        <th>الكاتب</th>
                        <th>دار النشر</th>
                        <th>المكتبة</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td>{{$book->title}}</td>
                            <td>{{$book->category->name}}</td>
                            <td>{{$book->author->name}}</td>
                            <td>{{$book->publisher->name}}</td>
                            <td>{{$book->library->name}}</td>
                            <td><a href="{{route('book-edit', $book->id)}}">مشاهدة</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $books->render() !!}
            @else
                <div class="alert alert-warning">
                    يظهر انه لا يوجد اي كتاب بعد.
                    <a href="{{route('book-create')}}">أضغط هنا</a> لإدراج كتاب جديد
                </div>
            @endif
        </div>
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="{{route('book-create')}}"><i class="far fa-edit fa-4x"></i></a>
    </div>

@endsection