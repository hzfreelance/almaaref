@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>{{$chapter->name}}</h3>
        </div>
    </div>

    <div class="row">
        @if(count($chapter->pages) > 0)
            @foreach($chapter->pages as $page)
                <div class="col-md-2 text-center">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route('book-view-page', [$chapter->book_id,$chapter->id,$page->id])}}">
                                <h3>ص {{$page->sort_order}}</h3>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <div class="alert alert-warning">لا يوجد صفحات</div>
            </div>
        @endif
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="{{route('book-create-page', $chapter->id)}}"><i class="far fa-edit fa-4x"></i></a>
    </div>

@endsection