@extends('layouts.app')

@section('content')
    <form method="POST" action="{{route('category-update', $category->id)}}" enctype="multipart/form-data"
          id="form-update">
        <div class="row">

            @csrf
            @method('put')
            <div class="col-md-12">
                <p>
                    <label for="name">الاسم</label>
                    <input type="text" class="form-control" placeholder="الاسم" name="name" value="{{$category->name}}">
                </p>
            </div>
            <div class="col-md-4">
                <div style="background: url('{{url('/').'/'.$category->image}}'); background-size: contain; background-repeat: no-repeat; height: 200px; background-position: center">

                </div>
            </div>
            <div class="col-md-8">
                <p>
                    <label for="image">الصورة</label>
                    <input type="file" id="image" name="image" class="form-control">
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="sort_order">الترتيب</label>
                    <input type="number" class="form-control" placeholder="الترتيب" id="sort_order" name="sort_order"
                           value="{{$category->sort_order}}">
                </p>
            </div>
            <div class="col-md-6">
                <p>
                    <label for="status">الحالة</label>
                    <select name="status" class="form-control">
                        <option value="1" {{ ($category->status == 1) ? 'selected' : ''  }}>منشور</option>
                        <option value="0" {{ ($category->status == 0) ? 'selected' : ''  }}>غير منشور</option>
                    </select>
                </p>
            </div>
            <div class="col-md-6">
                <input type="hidden" name="library_id" value="{{$category->library_id}}">
                <p>
                    <label for="library_id">المكتبة</label>
                    <select class="form-control" name="library_id" id="library_id" disabled="disabled">
                        @foreach($libraries as $library)
                            @if($library->id != 3)
                                <option value="{{$library->id}}" {{($library->id == $category->library_id) ? 'selected' : ''}}>{{$library->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </p>
            </div>
            @if($category->library_id != 1)
            <div class="col-md-6">
                <p>
                    <label for="parent_id">تابع لفئة</label>
                    <select class="form-control" name="parent_id" id="parent_id">
                        <option value="0" {{($category->parent_id == 0) ? 'selected' : ''}}>غير تابع</option>
                        @foreach($categories as $categoryp)
                            @if(count($categoryp->children) == 0 && $categoryp->library_id == 2)
                                <option value="{{$categoryp->id}}" {{($categoryp->id == $category->parent_id) ? 'selected' : ''}}>
                                    @if(isset($categoryp->parent->parent->parent->parent->parent->name))
                                        {{$categoryp->parent->parent->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($categoryp->parent->parent->parent->parent->name))
                                        {{$categoryp->parent->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($categoryp->parent->parent->parent->name))
                                        {{$categoryp->parent->parent->parent->name}} ->
                                    @endif
                                    @if(isset($categoryp->parent->parent->name))
                                        {{$categoryp->parent->parent->name}} ->
                                    @endif
                                    @if(isset($categoryp->parent->name))
                                        {{$categoryp->parent->name}} ->
                                    @endif
                                    {{$categoryp->name}}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </p>
            </div>
            @endif
            <div class="col-md-4 offset-8 text-left">
                <button type="submit" class="btn btn-success form-control">حفظ</button>
            </div>

        </div>
    </form>
@endsection