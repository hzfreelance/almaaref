@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if(count($categories) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>الاسم</th>
                        <th>الحالة</th>
                        <th>الترتيب</th>
                        <th>تابع لفئة</th>
                        <th>المكتبة</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>
                                {{$category->name}}
                            </td>
                            <td>
                                {{ ($category->status == 1) ? 'منشور' : 'غير منشور' }}
                            </td>
                            <td>
                                {{$category->sort_order}}
                            </td>
                            <td>
                                @if($category->parent_id == 0)
                                    غير تابع
                                @else
                                    {{$category->parent->name}}
                                @endif
                            </td>
                            <td>
                                {{$category->library->name}}
                            </td>
                            <td>
                                <a href="{{route('category-edit', $category->id)}}">مشاهدة</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $categories->render() !!}
            @else
                <div class="alert alert-warning">لا يوجد نتائج، <a data-toggle="modal" data-target="#modal-create"
                                                                   href="#"> انشاء جديد</a></div>

            @endif
        </div>
    </div>

    <div style="position: fixed; bottom: 50px; left: 50px;">
        <a href="#" data-toggle="modal" data-target="#modal-create"><i class="far fa-edit fa-4x"></i></a>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-create">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">فئة جديدة</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('category-store')}}" enctype="multipart/form-data"
                          id="form-create">
                        @csrf
                        <p>
                            <label for="name">الاسم</label>
                            <input type="text" class="form-control" placeholder="الاسم" name="name">
                        </p>
                        <p>
                            <label for="image">الصورة</label>
                            <input type="file" id="image" name="image" class="form-control">
                        </p>
                        <p>
                            <label for="sort_order">الترتيب</label>
                            <input type="number" class="form-control" placeholder="الترتيب" id="sort_order"
                                   name="sort_order">
                        </p>
                        <p>
                            <label for="library_id">المكتبة</label>
                            <select class="form-control" name="library_id" id="library_id">
                                @foreach($libraries as $library)
                                    @if($library->id != 3)
                                        <option value="{{$library->id}}">{{$library->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </p>
                        <p style="display: none" id="parent_category_section">
                            <label for="parent_id">تابع لفئة</label>
                            <select class="form-control" name="parent_id" id="parent_id">
                                <option value="0">غير تابع</option>
                                @foreach($categoriesInDropDown as $category)
                                    @if(count($category->children) == 0 && $category->library_id == 2)
                                        <option value="{{$category->id}}">
                                            @if(isset($category->parent->parent->parent->parent->parent->name))
                                                {{$category->parent->parent->parent->parent->parent->name}} ->
                                            @endif
                                            @if(isset($category->parent->parent->parent->parent->name))
                                                {{$category->parent->parent->parent->parent->name}} ->
                                            @endif
                                            @if(isset($category->parent->parent->parent->name))
                                                {{$category->parent->parent->parent->name}} ->
                                            @endif
                                            @if(isset($category->parent->parent->name))
                                                {{$category->parent->parent->name}} ->
                                            @endif
                                            @if(isset($category->parent->name))
                                                {{$category->parent->name}} ->
                                            @endif
                                            {{$category->name}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </p>
                        <p>
                            <label for="status">الحالة</label>
                            <select name="status" class="form-control">
                                <option value="1">منشور</option>
                                <option value="0">غير منشور</option>
                            </select>
                        </p>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $('#library_id').on('change', function () {
            if ($(this).val() == 2) {
                $('#parent_category_section').show();
            } else {
                $('#parent_category_section').hide();
            }
        });
    </script>

    <script>
        $("#form-create").submit(function (e) {

            $.ajax({
                url: '{{route('category-store')}}',
                type: 'POST',
                data: new FormData(this),
                cache: false,
                datatype: 'JSON',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.status == 1) {
                        location.reload();
                    }
                }, error: function () {

                }
            });

            e.preventDefault();
        });

    </script>

@endsection