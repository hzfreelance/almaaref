@extends('layouts.app')

@section('content')

<div class="row text-center">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h3>الكتب</h3>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="text text-success">{{$statsSummary['book_published']}}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text text-danger">{{$statsSummary['book_unpublished']}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h3>الكتّاب</h3>
                <h4 class="text text-success">{{$statsSummary['author']}}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h3>دور النشر</h3>
                <h4 class="text text-success">{{$statsSummary['publisher']}}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h3>الفئات</h3>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="text text-success">{{$statsSummary['category_published']}}</h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text text-danger">{{$statsSummary['category_unpublished']}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
