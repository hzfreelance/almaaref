<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>

    <link href="{{asset('selectize/dist/css/selectize.default.css')}}" rel="stylesheet">

</head>
<body>
@auth
    @include('partials.navbar')

    <div class="container rtl">
        <div class="row" style="margin-top: 10px">
            <div class="col-md-2">
                @include('partials.side-navigation')
            </div>
            <div class="col-md-10">
                @if(Session::has('success'))
                    <div class="alert alert-success">{{Session('success')}}</div>
                @endif

                @if(Session::has('error'))
                    <div class="alert alert-error">{{Session('error')}}</div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(isset($breadcrumb))
                    {!! $breadcrumb !!}
                @endif

                @yield('content')
            </div>
        </div>
    </div>
@endauth

@guest

    <div class="container">
        <div class="row" style="margin-top:70px">
            @yield('content')
        </div>
    </div>

@endguest

<script src="{{asset('selectize/dist/js/standalone/selectize.min.js')}}"></script>

@yield('script')

</body>
</html>
