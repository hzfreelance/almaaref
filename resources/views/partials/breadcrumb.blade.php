{{--<nav aria-label="breadcrumb">--}}
{{--<ol class="breadcrumb">--}}
{{--@foreach(config('breadcrumb.'.\Request::route()->getName()) as $key => $value)--}}
{{--@if($value == \Request::route()->getName())--}}
{{--<li class="breadcrumb-item active" aria-current="page">--}}
{{--{{$key}}--}}
{{--</li>--}}
{{--@else--}}
{{--<li class="breadcrumb-item" }}>--}}
{{--<a href="{{route($value)}}">{{$key}}</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@endforeach--}}
{{--</ol>--}}
{{--</nav>--}}

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach($levels as $value)
            @if($value['name'] == \Request::route()->getName())
                <li class="breadcrumb-item active" aria-current="page">
                    {{$value['label']}}
                </li>
            @else
                <li class="breadcrumb-item" }}>
                    @if(isset($value['parameters']))
                        <a href="{{route($value['name'], $value['parameters'])}}">{{$value['label']}}</a>
                    @else
                        <a href="{{route($value['name'])}}">{{$value['label']}}</a>
                    @endif
                </li>
            @endif
        @endforeach
    </ol>
</nav>

