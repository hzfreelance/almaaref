<div class="list-group">
    <a href="{{route('home')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('home')) ? 'active' : '' }}">
        الرئيسية
    </a>
    <a href="{{route('book-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('book-index')) ? 'active' : '' }}">
        الكتب
    </a>
    <a href="{{route('section-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('section-index')) ? 'active' : '' }}">
        الأقسام
    </a>
    <a href="{{route('category-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('category-index')) ? 'active' : '' }}">
        الفئات
    </a>
    <a href="{{route('author-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('author-index')) ? 'active' : '' }}">
        الكتّاب
    </a>
    <a href="{{route('publisher-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('publisher-index')) ? 'active' : '' }}">
        دور النشر
    </a>
    <a href="{{route('banner-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('banner-index')) ? 'active' : '' }}">
      الاعلانات
    </a>
    <a href="{{route('library-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('library-index')) ? 'active' : '' }}">
        المكتبات
    </a>
    <a href="{{route('article-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('article-index')) ? 'active' : '' }}">
        المقالات
    </a>
    <a href="{{route('article-category-index')}}" class="list-group-item list-group-item-action {{ (\Request::route()->named('article-category-index')) ? 'active' : '' }}">
        فئات المقالات
    </a>
</div>