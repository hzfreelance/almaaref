@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <p>
                <label for="search">
                    اختر الكتب التي تريد اضافتها الى هذا القسم
                </label>

            </p>
        </div>
        <div class="col-md-10">
            <p>
                <select multiple name="search[]" id="search">
                    @foreach($books as $book)
                        <option {{(in_array($book->id, $section->books->pluck('id')->toArray())) ? 'selected' : ''}} value="{{$book->id}}">{{$book->title}}</option>
                    @endforeach
                </select>
            </p>
        </div>
        <div class="col-md-2">
            <p>
                <button class="form-control btn btn-primary" onclick="addBooks()">اضافة</button>
            </p>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $('#search').selectize({
            multiple: true
        });

        function addBooks(){
            booksIds = $('#search').val();
            $.ajax({
                url: '{{route('section-add-books',$section->id)}}',
                type: 'POST',
                data:{
                    _token: '{{csrf_token()}}',
                    booksIds: booksIds
                },
                cache: false,
                datatype: 'JSON',
                success: function(data){
                    if(data.status == 1){
                        alert('{{MESSAGE_GENERAL_UPDATE_SUCCESS}}');
                    }
                },error:function(){

                }
            })
        }

    </script>

@endsection
