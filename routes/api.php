<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api'], function(){
    Route::get('get-books','ApiController@getBooks');
    Route::get('find-book', 'ApiController@findABook');
    Route::get('get-book/{id}', 'ApiController@getBook');

    Route::get('get-books-by-categories', 'ApiController@getBooksByCategories');
    Route::get('get-category-with-books/{id}', 'ApiController@getCategoryWithBooks');

    Route::get('get-book-chapters/{id}', 'ApiController@getBookChapters');
    Route::get('get-book-chapter-pages/{id}', 'ApiController@getBookChapterPages');
    Route::get('get-book-chapter-page/{id}', 'ApiController@getBookChapterPage');
    Route::get('get-book-details/{id}', 'ApiController@getBookDetails');

    Route::get('get-sections', 'ApiController@getSections');
    Route::get('get-section-books/{id}', 'ApiController@getSectionBooks');

    Route::get('get-latest-banner', 'ApiController@getLastBanner');

    Route::get('get-categories-levels/{library_id}', 'ApiController@getCategoriesLevels');

    Route::get('get-articles', 'ApiController@getArticles');
    Route::get('get-article/{id}', 'ApiController@getArticle');
});