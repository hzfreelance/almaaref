<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('sync-db', 'HomeController@syncDb');

Route::group(['middleware' => 'auth'], function () {

    // Datatabes Routes
    Route::group(['namespace' => 'Book'], function () {
        Route::get('datatable/books', 'BookController@datatable')->name('book-datatable');
    });

    // Book Model Routes
    Route::group(['namespace' => 'Book'], function () {
        Route::resource('book', 'BookController')->names([
            'index' => 'book-index',
            'create' => 'book-create',
            'store' => 'book-store',
            'edit' => 'book-edit',
            'update' => 'book-update',
            'destroy' => 'book-destroy'
        ]);

        Route::post('book/create-chapter', 'BookController@createChapter')->name('book-create-chapter');
        Route::get('book/view-chapter/{id}', 'BookController@viewChapter')->name('book-view-chapter');
        Route::get('book/chapter/{id}/create-page', 'BookController@createPage')->name('book-create-page');
        Route::post('book/chapter/{id}/create-page', 'BookController@storePage')->name('book-store-page');
        Route::get('book/{bid}/chapter/{cid}/page/{pid}', 'BookController@viewPage')->name('book-view-page');
        Route::put('book/{bid}/chapter/{cid}/page/{pid}', 'BookController@updatePage')->name('book-update-page');

    });
    // BookCategory Model Routes
    Route::group(['namespace' => 'Category'], function () {
        Route::resource('category', 'CategoryController')->names([
            'index' => 'category-index',
            'create' => 'category-create',
            'store' => 'category-store',
            'edit' => 'category-edit',
            'update' => 'category-update',
            'destroy' => 'category-destroy'
        ]);
    });

    // BookCategory Model Routes
    Route::group(['namespace' => 'Author'], function () {
        Route::resource('author', 'AuthorController')->names([
            'index' => 'author-index',
            'create' => 'author-create',
            'store' => 'author-store',
            'edit' => 'author-edit',
            'update' => 'author-update',
            'destroy' => 'author-destroy'
        ]);
    });

    // BookCategory Model Routes
    Route::group(['namespace' => 'Publisher'], function () {
        Route::resource('publisher', 'PublisherController')->names([
            'index' => 'publisher-index',
            'create' => 'publisher-create',
            'store' => 'publisher-store',
            'edit' => 'publisher-edit',
            'update' => 'publisher-update',
            'destroy' => 'publisher-destroy'
        ]);
    });

    Route::group(['namespace' => 'Section'], function(){
       Route::resource('section', 'SectionController')->names([
          'index' => 'section-index',
           'store' => 'section-store',
           'update' => 'section-update',
           'show' => 'section-show'
       ]);
       Route::post('add-books-to-section/{id}','SectionController@addBooks')->name('section-add-books');
    });

    Route::group(['namespace' => 'Banner'],function(){
        Route::resource('banner', 'BannerController')->names([
            'index' => 'banner-index',
            'store' => 'banner-store',
            'update' => 'banner-update',
            'show' => 'banner-show'
        ]);
    });

    Route::group(['namespace' => 'Library'], function(){
       Route::resource('library', 'LibraryController')->names([
           'index' => 'library-index',
           'store' => 'library-store',
           'update' => 'library-update',
           'show' => 'library-show'
       ]);
    });

    Route::group(['namespace' => 'Article'], function(){
        Route::resource('article', 'ArticleController')->names([
            'index' => 'article-index',
            'store' => 'article-store',
            'update' => 'article-update',
            'show' => 'article-show',
            'create' => 'article-create',
            'edit' => 'article-edit'
        ]);
    });

    //Article category
    Route::group(['namespace' => 'ArticleCategory'], function () {
        Route::resource('article-category', 'ArticleCategoryController')->names([
            'index' => 'article-category-index',
            'create' => 'article-category-create',
            'store' => 'article-category-store',
            'edit' => 'article-category-edit',
            'update' => 'article-category-update',
            'destroy' => 'article-category-destroy'
        ]);
    });

    // Misc images
    Route::group(['namespace' => 'MiscImage'], function(){
       Route::resource('misc-images', 'MiscImageController');
    });

});

